//      main.cpp
//
//      Copyright 2010 Agorabox <bastien.bouzerau@agorabox.org>
//
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.

#include "../config.h"

#include <glib/gi18n.h>
#include <moblin-panel/mpl-panel-common.h>
#include <moblin-panel/mpl-panel-qt.h>
#include <QApplication>
#include <QtGui/QWidget>
#include <stdlib.h>
#include <stdio.h>

#include <locale.h>
#define _(String) gettext (String)

#include "bloom-panel-web.h"

BloomPanelWeb *window = NULL;

void show_window ();
void hide_window ();

int main(int argc, char* argv[])
{
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
    bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
    textdomain(GETTEXT_PACKAGE);

    gtk_init(&argc, &argv);

    MplPanelClient *panel_client;
    QApplication app(argc, argv);

    panel_client = mpl_panel_qt_new (MPL_PANEL_INTERNET,
                                     _("Internet"),
                                     THEMEDIR"/toolbar-button.css",
                                     "unknown",
                                     TRUE);

    mpl_panel_client_request_button_style (panel_client, "internet");
    g_signal_connect(panel_client, "show", G_CALLBACK (show_window), NULL);

    window = new BloomPanelWeb(panel_client, mpl_panel_qt_get_window ((MplPanelQt*) panel_client));
    mpl_panel_qt_set_child (panel_client, window);

    return app.exec();
}

void
show_window ()
{
    window->launch_web_browser ();
}
