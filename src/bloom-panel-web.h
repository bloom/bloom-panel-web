//      BloomPanelWeb.h
//
//      Copyright 2010 Agorabox <bastien.bouzerau@agorabox.org>
//
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.

#ifndef HEADER_PANEL_WEB
#define HEADER_PANEL_WEB
#define __GTK_BINDINGS_H__

#include <glib/gi18n.h>
#include <QtGui/QWidget>
#include <QtCore/QProcess>
#include <QX11EmbedContainer>
#include <moblin-panel/mpl-panel-common.h>
#include <moblin-panel/mpl-panel-qt.h>

#define __GTK_BINDINGS_H__
#define WNCK_I_KNOW_THIS_IS_UNSTABLE
#include <libwnck/libwnck.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <gdk/gdk.h>
#include <gdk/gdkx.h>

class BloomPanelWeb : public QWidget
{
    Q_OBJECT

public:
    BloomPanelWeb      (MplPanelClient *panel_client, QWidget *parent = 0);
    void                launch_web_browser ();
    void                hide_web_browser ();
    QMovie              *movie;
    QX11EmbedContainer  *container;

public slots:
    void                process_finished ();
    void                ensure_size ();
    void                showClientEmbedded ();
    void                showClientClosed ();

private:
    MplPanelClient      *panel;
    QLayout             *main_layout;
    QWidget             *widget;
    QProcess process;

public:
    static BloomPanelWeb *instance;
};

#endif
