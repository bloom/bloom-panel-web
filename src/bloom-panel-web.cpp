//      bloom-panel-web.cpp
//
//      Copyright 2010 Agorabox.org <bastien.bouzerau@agorabox.org>
//
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.

#include <QObject>
#include <QX11Info>
#include <signal.h>
#include <gdk/gdk.h>

#include <sys/types.h>
#include <unistd.h>

#include "bloom-panel-web.h"

#define PROP_MOTIF_WM_HINTS_ELEMENTS 5
#define MWM_HINTS_DECORATIONS (1L << 1)

#define MAXPIDLEN 10
#define MAXPROCPATHLEN (MAXPIDLEN + 11)

static void     window_opened_cb(WnckScreen *screen, WnckWindow *window);

static Atom net_wm_window_state_skip_tb             = None;
static Atom net_wm_window_state                     = None;

int              browser_pid    = 0;
Window           browser_xid    = 0;

BloomPanelWeb* BloomPanelWeb::instance = NULL;

BloomPanelWeb::BloomPanelWeb(MplPanelClient *panel_client, QWidget *parent) : QWidget(parent), panel(panel_client)
{
    instance = this;

    setObjectName (QString::fromUtf8 ("main_layout"));

    main_layout = new QVBoxLayout (this);
    main_layout->setContentsMargins (0,0,0,0);
    main_layout->setSpacing (0);

    net_wm_window_state = XInternAtom (gdk_display,
                                       "_NET_WM_STATE",
                                       False);

    net_wm_window_state_skip_tb = XInternAtom (gdk_display,
                                               "_NET_WM_STATE_SKIP_TASKBAR",
                                               False);

    XChangeProperty (gdk_display, winId (), net_wm_window_state,
                     XA_ATOM, 32, PropModeAppend,
                     (unsigned char *) &net_wm_window_state_skip_tb, 1);

    const int num_screens = gdk_display_get_n_screens (gdk_display_get_default ());
    for (int i = 0 ; i < num_screens; ++i) {
        WnckScreen *screen = wnck_screen_get (i);
        g_signal_connect (screen, "window-opened", (GCallback) window_opened_cb, NULL);
    }

    QObject::connect (&process, SIGNAL (finished (int, QProcess::ExitStatus)),
                      this, SLOT (process_finished ()));

    container = new QX11EmbedContainer (this);
    container->hide ();
    main_layout->addWidget (container);

    connect (container, SIGNAL (clientIsEmbedded ()), SLOT(showClientEmbedded ()));
    connect (container, SIGNAL (clientClosed ()), SLOT(showClientClosed ()));

    process.start ("firefox");
    browser_pid = process.pid ();
}

void
BloomPanelWeb::launch_web_browser ()
{
    if (!browser_xid) {
        while (!browser_xid || container->clientWinId() == 0) {
            QEventLoop loop;
            loop.processEvents (QEventLoop::AllEvents);
        }
        container->show ();
    }
}

void
BloomPanelWeb::process_finished ()
{
    browser_xid = 0;
    process.start ("firefox");
    browser_pid = process.pid ();
}

void
BloomPanelWeb::ensure_size ()
{
	parentWidget ()->resize (parentWidget ()-> width(), parentWidget ()-> height() + 1);
}

void
BloomPanelWeb::showClientEmbedded ()
{
    QTimer::singleShot(1000, this, SLOT(ensure_size ()));
}

void
BloomPanelWeb::showClientClosed ()
{
    container->embedClient (browser_xid);
}

static void
window_opened_cb (WnckScreen *screen, WnckWindow *window)
{
    if (wnck_window_get_pid (window) == 0) {
        return;
    }

    if (!strcmp (wnck_application_get_name (wnck_window_get_application (window)), "Firefox") &&
        !browser_xid &&
        0 == wnck_window_get_window_type (window)) {
        browser_xid = wnck_window_get_xid (window);

        struct hints_struct {
            unsigned long flags;
            unsigned long functions;
            unsigned long decorations;
            long inputMode;
            unsigned long status;
        };
	
        struct hints_struct hints;
        hints.flags = MWM_HINTS_DECORATIONS;
        hints.decorations = 0;

        XChangeProperty (QX11Info::display (), browser_xid,
	        XInternAtom (QX11Info::display (), "_MOTIF_WM_HINTS", FALSE),
	        XInternAtom (QX11Info::display (), "_MOTIF_WM_HINTS", FALSE), 32, PropModeReplace,
			(unsigned char *)&hints, PROP_MOTIF_WM_HINTS_ELEMENTS);

        XFlush (QX11Info::display ());

        BloomPanelWeb::instance->container->embedClient (browser_xid);
    }
}
